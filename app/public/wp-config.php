<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GhscAx1rRrd5YoCC0cANRDMiSmn9fXs418P7FugJC6yqDGNtmxjcsDvg0syA7yjL68eMTT0x+JoTwuZa0hMV7w==');
define('SECURE_AUTH_KEY',  'OQ39GPonLgvUhUSgoWTJwMuZFj3ONN8cFXxAlwaqzJQiS/7YGX9QaQAPEY4ax29Pg9clZRIKVqYmIkStp7ejnA==');
define('LOGGED_IN_KEY',    'eb5k9xcoyWoFBoxeQ/dN836AFPxfIUR3RUhbP2O8svCyWvH0rhpRjb6luxNfcmo1Oc6+FElwXUmy6A615WMESg==');
define('NONCE_KEY',        'cKaSgXFF7Ack9e9dA2c+wekcSZQH13o1uhK78aHnUy/Ecr602hsU2nCtXFkTdzLIW2NmEj+wOwO/BG8VGlmG6A==');
define('AUTH_SALT',        'P4IpJzMD+m8IzMhkIvHt07D79Z+XD0DoLvY2pv8nQr7BEEgRKE2jqVZkAse1u5Ptd4qczMsOjO8aan7aznyZfA==');
define('SECURE_AUTH_SALT', 'ScKAevVBY7xUA/oMJ2ARL4PoMdtwPXehjhLu54TdfQi5H9w7HU6Hw5Pgsiv9eACFtKhHkj67FiZRQi+xHu69BQ==');
define('LOGGED_IN_SALT',   'TgwmBl4HjjD90gNogaTTvhlnKY9/WOQs6vOUD1oZLZLfpcmAvT2SKMfROvfMA5m3+xqyH1Kwlz9guQ4ejCiqzQ==');
define('NONCE_SALT',       '7OxfFPHEe38qTiGxYgOyd2MF4/9sCiN+04zi4eCDQunxnRQejXT3nQ189XOqpi1RswHaewqRNDacdxN90rMV6Q==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
